<?php

namespace App\Bitm\SEIP136104\Birthday;

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\Message\Message;

class Birthday{
    public $id='';
    public $date='';
    public $name='';
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","basis_atomicproject");
    }


    public function prepare($data=''){
        if(array_key_exists("id",$data))
            $this->id=$data['id'];

        if(array_key_exists("date",$data))
            $this->date=$data['date'];

        if(array_key_exists("name",$data))
            $this->name=$data['name'];
    }


    /*public function index(){
        $allCourse=array();
        $query = "SELECT * FROM `birthday` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
            $allCourse[]=$row;

        return $allCourse;
    }*/


    public function paginator($startFrom=0,$limit=5){
        $hobbies = array();
        $query = "SELECT * FROM birthday WHERE `deleted_at` IS NULL  LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `basis_atomicproject`.`birthday` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }


    public function store(){
        //Utility::dd($this->date);
        $query = "INSERT INTO `basis_atomicproject`.`birthday` (`name`, `date`) VALUES ('".$this->name."', '".$this->date."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR!";
    }


    public function view(){
        $single=array();
        $query = "SELECT * FROM  `birthday` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        //
        if($row=mysqli_fetch_object($result))
            $single=$row;

        return $single;
    }


    public function update(){
       // echo $this->date;
        //die();
        $query = "UPDATE `basis_atomicproject`.`birthday` SET `name` = '".$this->name."', `date` = '".$this->date."' WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function trash(){
        $query = "UPDATE `basis_atomicproject`.`birthday` SET `deleted_at` = ".time()." WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function recover(){
        $query = "UPDATE `birthday` SET `deleted_at`= NULL WHERE `birthday`.`id` = ".$this->id;
        //echo $this->id.$this->hobby;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }
        else
            echo "ERROR!";
    }


    public function delete(){
        $query = "DELETE FROM `basis_atomicproject`.`birthday` WHERE `birthday`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function trashed(){
        $allCourse=array();
        $query = "SELECT * FROM `birthday` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
            $allCourse[]=$row;

        return $allCourse;
    }


    public function recoverSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);
            ///Utility::dd($ids);
            $query = "UPDATE `basis_atomicproject`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }

    }

    public function deleteSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);

            $query = "DELETE FROM `basis_atomicproject`.`birthday` WHERE `birthday`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }
    }
}