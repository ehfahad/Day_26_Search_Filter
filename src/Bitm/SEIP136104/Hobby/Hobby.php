<?php

namespace App\Bitm\SEIP136104\Hobby;

use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

class Hobby{
    public $id= '';
    public $hobby = '';
    public $conn;


    public function __construct()
    { 
        $this->conn = mysqli_connect("localhost","root","","basis_atomicproject");
    }


    public function prepare($data){
        if(array_key_exists("id",$data))
            $this->id= $data['id'];

        if(array_key_exists("hobby",$data))
            $this->hobby= $data['hobby'];
    }

    
    /*public function index(){
        $hobbies = array();
        $query = "SELECT * FROM hobbies WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }*/


    public function paginator($startFrom=0,$limit=5){
        $hobbies = array();
        $query = "SELECT * FROM hobbies WHERE `deleted_at` IS NULL  LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `basis_atomicproject`.`hobbies` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }


    public function store(){
        $query = "INSERT INTO `hobbies` (`hobby`) VALUES ('".$this->hobby."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function view(){
        $query = "SELECT `hobby` FROM hobbies WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if(!$result)
            mysqli_error($this->conn);
        if($row = mysqli_fetch_assoc($result)){
            $singleHobby = explode(",",$row['hobby']);
            // Utility::d($singleHobby);
            return $singleHobby;
        }

        else
            echo "ERROR!";
    }


    public function update(){
        $query = "UPDATE `hobbies` SET `hobby` = '".$this->hobby."' WHERE `hobbies`.`id` = ".$this->id;
        //echo $this->id.$this->hobby;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
            
        else
            echo "ERROR!";
    }


    public function trash(){
        $query = "UPDATE `hobbies` SET `deleted_at` = '".time()."' WHERE `hobbies`.`id` = ".$this->id;
        //echo $this->id.$this->hobby;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }
           
        else
            echo "ERROR!";
    }


    public function trashed(){
        $hobbies = array();
        $query = "SELECT * FROM hobbies WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function delete(){
        $query ="DELETE FROM `hobbies` WHERE `hobbies`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR!";
    }


    public function recover(){
        $query = "UPDATE `hobbies` SET `deleted_at`= NULL WHERE `hobbies`.`id` = ".$this->id;
        //echo $this->id.$this->hobby;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }
        else
            echo "ERROR!";
    }


    public function recoverSelected($IDs= array()){
        if(is_array($IDs) && count($IDs)>0){
            $ids = implode(",",$IDs);
            //echo $ids;
            $query = "UPDATE `hobbies` SET `deleted_at`= NULL WHERE `hobbies`.`id` IN (".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }
            else 
                echo "ERROR!";
        }
        else
            Utility::redirect("trashlist.php");
    }


    public function deleteSelected($IDs= array()){
        if(is_array($IDs) && count($IDs)>0){
            $ids = implode(",",$IDs);
           // echo $ids;
            $query = "DELETE FROM `hobbies` WHERE `hobbies`.`id` IN (".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR!";
        }
        else
            Utility::redirect("trashlist.php");
    }

}