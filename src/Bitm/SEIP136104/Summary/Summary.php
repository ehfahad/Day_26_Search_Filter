<?php
namespace App\Bitm\SEIP136104\Summary;

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\Message\Message;

class Summary{
    public $id="";
    public $org_name="";
    public $summary="";
    public $deleted_at="";
    public $conn;



    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "basis_atomicproject") or die("Database connection failed");
    }


    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->org_name = $data['name'];
        }

        if (array_key_exists("summary", $data)) {
            $this->summary = $data['summary'];
        }

        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
    }


    /*public function index(){
        $_allinfo=array();
        $query="SELECT * FROM `basis_atomicproject`.`organization` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allinfo[] = $row;
        }

        return $_allinfo;
    }*/


    public function paginator($startFrom=0,$limit=5){
        $organization = array();
        $query = "SELECT * FROM `organization` WHERE `deleted_at` IS NULL  LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $organization[]=$row;
        return $organization;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `basis_atomicproject`.`organization` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }


    public function store(){
        $query="INSERT INTO `basis_atomicproject`.`organization` (`org_name`, `summary`) VALUES ('".$this->org_name."', '".$this->summary."')";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";

    }


    public function view()
    {
        $query = "SELECT * FROM `organization` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function update(){
        $query = "UPDATE `organization` SET `org_name` = '".$this->org_name."', `summary` = '".$this->summary."' WHERE `organization`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function trash(){
        $query = "UPDATE `organization` SET `deleted_at` = '".time()."' WHERE `organization`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function trashed(){
        $organization = array();
        $query = "SELECT * FROM `organization` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $organization[]=$row;
        return $organization;
    }


    public function delete(){
        $query ="DELETE FROM `organization` WHERE `organization`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR!";
    }


    public function recover(){
        $query = "UPDATE `organization` SET `deleted_at`= NULL WHERE `organization`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }
        else
            echo "ERROR!";
    }


    public function recoverSelected($IDs= array()){
        if(is_array($IDs) && count($IDs)>0){
            $ids = implode(",",$IDs);
            //echo $ids;
            $query = "UPDATE `organization` SET `deleted_at`= NULL WHERE `organization`.`id` IN (".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR!";
        }
        else
            Utility::redirect("trashlist.php");
    }


    public function deleteSelected($IDs= array()){
        if(is_array($IDs) && count($IDs)>0){
            $ids = implode(",",$IDs);
            // echo $ids;
            $query = "DELETE FROM `organization` WHERE `organization`.`id` IN (".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR!";
        }
        else
            Utility::redirect("trashlist.php");
    }
}