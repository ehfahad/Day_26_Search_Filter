<?php

namespace App\Bitm\SEIP136104\Email;

use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;


class Email
{
    public $id="";
    public $name="";
    public $email="";
    public $deleted_at;
    public $conn;
    
    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","basis_atomicproject");
    }
    
    public function prepare($data){
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists("name",$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists("email",$data)){
            $this->email=$data['email'];
        }
    }


    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `basis_atomicproject`.`subscribers` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allS = array();
        $query="SELECT * FROM `subscribers` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allS[] = $row;
        }

        return $_allS;

    }


    public function store(){
        $query="INSERT INTO  `subscribers` (`username`,`email`) VALUES ('".$this->name."','".$this->email."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR!";
    }


    public function view(){
        $single=array();
        $query = "SELECT * FROM  `subscribers` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        //
        if($row=mysqli_fetch_object($result))
            $single=$row;

        return $single;
    }


    public function update(){
        $query = "UPDATE `basis_atomicproject`.`subscribers` SET `username` = '".$this->name."', `email` = '".$this->email."' WHERE `subscribers`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function trash(){
        $query = "UPDATE `basis_atomicproject`.`subscribers` SET `deleted_at` = ".time()." WHERE `subscribers`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function recover(){
        $query = "UPDATE `subscribers` SET `deleted_at`= NULL WHERE `subscribers`.`id` = ".$this->id;
        //echo $this->id.$this->hobby;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }
        else
            echo "ERROR!";
    }


    public function delete(){
        $query = "DELETE FROM `basis_atomicproject`.`subscribers` WHERE `subscribers`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function trashed(){
        $allCourse=array();
        $query = "SELECT * FROM `subscribers` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result))
            $allCourse[]=$row;

        return $allCourse;
    }


    public function recoverSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);
            ///Utility::dd($ids);
            $query = "UPDATE `basis_atomicproject`.`subscribers` SET `deleted_at` = NULL WHERE `subscribers`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }

    }

    public function deleteSelected($IDs=array()){
        if(count($IDs>0)){
            $ids= implode(",",$IDs);
            $query = "DELETE FROM `basis_atomicproject`.`subscribers` WHERE `subscribers`.`id` IN (".$ids.")";
            $result= mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }
    }
}