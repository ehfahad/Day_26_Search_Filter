<?php
    namespace App\Bitm\SEIP136104\booklist;

    use App\Bitm\SEIP136104\Message\Message;
    use App\Bitm\SEIP136104\Utility\Utility;
    class Book
    {
        public $title = "";
        public $id = "";
        public $conn;
        public $description="";
        public $filterByTitle="";
        public $filterByDescription="";
        public $search="";

        public function __construct()
        {
            $this->conn = mysqli_connect("localhost", "root", "", "basis_atomicproject");
        }

        public function prepare($data)
        {
            if (array_key_exists('title', $data))
                $this->title = $data['title'];
            if (array_key_exists('id', $data))
                $this->id = $data['id'];
            if (array_key_exists("description", $data)) {
                $this->description = $data['description'];
            }
            if (array_key_exists("filterByTitle", $data)) {
                $this->filterByTitle = $data['filterByTitle'];
            }
            if (array_key_exists("filterByDescription", $data)) {
                $this->filterByDescription = $data['filterByDescription'];
            }
            if (array_key_exists("search", $data)) {
                $this->search = $data['search'];
            }
        }


        public function index()
        {
            $whereClause= " 1=1 ";
            if(!empty($this->filterByTitle)){
                $whereClause.=" AND  title LIKE '%".$this->filterByTitle."%'";
            }
            if(!empty($this->filterByDescription)){
                $whereClause.=" AND  description LIKE '%".$this->filterByDescription."%'";
            }
            if(!empty($this->search)){
                $whereClause.=" AND  description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
            }


            $_allbook = array();
            $query = "SELECT * FROM `booklist` WHERE `deleted_at` IS NULL AND ".$whereClause;
            $result = mysqli_query($this->conn, $query);

            while ($row = mysqli_fetch_object($result)) {
                $_allbook[] = $row;
            }
            return $_allbook;
        }


        public function paginator($startFrom=0,$limit=5){
            $hobbies = array();
            $query = "SELECT * FROM `booklist` WHERE `deleted_at` IS NULL  LIMIT ".$startFrom.",".$limit;
            $result = mysqli_query($this->conn,$query);
            while($row= mysqli_fetch_object($result))
                $hobbies[]=$row;
            return $hobbies;
        }


        public function count(){
            $query = "SELECT COUNT(*) AS total FROM `basis_atomicproject`.`booklist` WHERE `deleted_at` IS NULL";
            $result = mysqli_query($this->conn,$query);
            if($row= mysqli_fetch_assoc($result))
                return $row['total'];
        }


        public function store()
        {
            $description_without_tags= strip_tags($this->description);
            $query = "INSERT INTO `basis_atomicproject`.`booklist` (`title`, `description`, `description_without_tag`) VALUES ('".$this->title."', '".$this->description."', '".$description_without_tags."')";
            $result = mysqli_query($this->conn, $query);

            if($result){
                Message::message("Data has been stored successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }


        public function trashed()
        {
            $_allbook = array();
            $query = "SELECT * FROM `booklist` WHERE `deleted_at` IS NOT NULL";
            $result = mysqli_query($this->conn, $query);

            while ($row = mysqli_fetch_object($result)) {
                $_allbook[] = $row;
            }
            return $_allbook;
        }


        public function view()
        {
            $query = "SELECT * FROM `basis_atomicproject`.`booklist` WHERE `id`=" . $this->id;
           // echo $query;
            $result = mysqli_query($this->conn, $query);
            if(!$result)
                echo "Error";
            $row = mysqli_fetch_object($result);
            return $row;
        }


        public function update()
        {
            //echo $this->id."<br>Title: ".$this->title;
            $query = "UPDATE `basis_atomicproject`.`booklist` SET `title` = '" . $this->title . "' WHERE `booklist`.`id` = " . $this->id;
            //echo $query;

            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been updated successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";

        }

        public function delete(){
            //echo $this->id."<br>Title: ".$this->title;
            $query = "DELETE FROM `booklist` WHERE `booklist`.`id` = ".$this->id;
            //echo $query;

            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }

        public function trash(){
            $query = "UPDATE `basis_atomicproject`.`booklist` SET `deleted_at` = '" .time(). "' WHERE `booklist`.`id` = " . $this->id;
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been sent to trash successfully");
                Utility::redirect("index.php");
            }
            else
                echo "ERROR";
        }
        
        public function recover(){
            $query = "UPDATE `basis_atomicproject`.`booklist` SET `deleted_at`=NULL WHERE `booklist`.`id` = " . $this->id;
            //echo $query;

            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("trashlist.php");
            }
            else
                echo "ERROR";
        }


        public function recoverSelected($IDs=array()){
            if(count($IDs>0)){
                $ids= implode(",",$IDs);
                ///Utility::dd($ids);
                $query = "UPDATE `basis_atomicproject`.`booklist` SET `deleted_at` = NULL WHERE `booklist`.`id` IN (".$ids.")";
                $result= mysqli_query($this->conn,$query);
                if($result){
                    Message::message("Data has been recovered successfully");
                    Utility::redirect("index.php");
                }
                else
                    echo "ERROR";
            }

        }


        public function deleteSelected($IDs=array()){
            if(count($IDs>0)){
                $ids= implode(",",$IDs);

                $query = "DELETE FROM `basis_atomicproject`.`booklist` WHERE `booklist`.`id` IN (".$ids.")";
                $result= mysqli_query($this->conn,$query);
                if($result){
                    Message::message("Data has been deleted successfully");
                    Utility::redirect("index.php");
                }
                else
                    echo "ERROR";
            }
        }


        public function getAllTitle()
        {
            $_allBook = array();
            $query = "SELECT `title` FROM `booklist` WHERE `deleted_at` IS NULL";
            $result = mysqli_query($this->conn, $query);
            while ($row = mysqli_fetch_assoc($result)) {
                $_allBook[] = $row['title'];
            }
            return $_allBook;


        }


        public function getAllDescription()
        {
            $_allBook = array();
            $query = "SELECT `description_without_tag` FROM `booklist` WHERE `deleted_at` IS NULL";
            $result = mysqli_query($this->conn, $query);
            while ($row = mysqli_fetch_assoc($result)) {
                $_allBook[] = $row['description_without_tag'];
            }
            return $_allBook;


        }


        public function getAllData()
        {
            $_allBook = array();
            $query = "SELECT `title`,`description_without_tag` FROM `booklist` WHERE `deleted_at` IS NULL";
            $result = mysqli_query($this->conn, $query);
            while ($row = mysqli_fetch_assoc($result)) {
                $_allBook[] = $row['description_without_tag'];
            }
            return $_allBook;


        }
    }