<?php

namespace App\Bitm\SEIP136104\City;

use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

class City
{
    public $id="";
    public $name="";
    public $city="";
    public $deleted_at;
    public $conn;


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "basis_atomicproject") or die("Database connection failed");
    }


    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("city", $data)) {
            $this->city = $data['city'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return  $this;

    }

    public function store(){
        $query="INSERT INTO `basis_atomicproject`.`cities` (`username`, `city`) VALUES ('".$this->name."', '".$this->city."')";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";

    }
    public function view(){
        $query="SELECT * FROM `basis_atomicproject`.`cities` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function update(){

        $query = "UPDATE `basis_atomicproject`.`cities` SET `username` = '" . $this->name . "', `city` = '" . $this->city . "' WHERE `cities`.`id` =" . $this->id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function delete(){
        $query="DELETE FROM `basis_atomicproject`.`cities`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";

    }


    public function trash()
    {
        $this->deleted_at= time();
        $query = "UPDATE `basis_atomicproject`.`cities` SET `deleted_at` =" . $this->deleted_at. " WHERE `cities`.`id` = " . $this->id;
        //echo $query;
        $result = mysqli_query($this->conn, $query);
        echo $result;
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR!";
    }

    public function trashed()
    {
        $_allpic = array();
        $query = "SELECT * FROM `cities` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allpic[] = $row;
        }
        return $_allpic;
    }

    public function recover()
    {

        $query = "UPDATE `basis_atomicproject`.`cities` SET `deleted_at` = NULL WHERE `cities`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }

        else
            echo "ERROR!";

    }


    public function recoverSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `basis_atomicproject`.`cities` SET `deleted_at` = NULL WHERE `cities`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }

            else
                echo "ERROR!";
        }
        else
            Utility::redirect("trashlist.php");

    }

    public function deleteSelected($IDs){
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "DELETE FROM `cities` WHERE `id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error deleting data");
                Utility::redirect("index.php");
            }
        }
        else
            Utility::redirect("trashlist.php");

    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `basis_atomicproject`.`cities` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }



    public function paginator($pageStartFrom=0,$Limit=5){
        $_allCity = array();
        $query="SELECT * FROM `cities` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allCity[] = $row;
        }

        return $_allCity;

    }

}