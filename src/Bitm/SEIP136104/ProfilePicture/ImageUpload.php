<?php

namespace App\Bitm\SEIP136104\ProfilePicture;

use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

class ImageUpload{
    public $id= '';
    public $name= '';
    public $img_name= '';
    public $conn;
    
    public function __construct()
    {
        $this->conn = mysqli_connect("localhost","root","","basis_atomicproject") or die("database connection failed");
    }
    
    
    public function prepare($data = ''){
        if(array_key_exists("id",$data))
            $this->id = $data['id'];

        if(array_key_exists("name",$data))
            $this->name = $data['name'];

        if(array_key_exists("image",$data))
            $this->img_name = $data['image'];
    }
    
    
    public function index(){
        $allInfo = array();
        $query = "SELECT * FROM `profilepicture` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result))
            $allInfo[] = $row;
        
        return $allInfo;
    }


    public function paginator($startFrom=0,$limit=5){
        $hobbies = array();
        $query = "SELECT * FROM profilepicture WHERE `deleted_at` IS NULL  LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `basis_atomicproject`.`profilepicture` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }


    public function view(){
        $query = "SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($row=mysqli_fetch_object($result))
            return $row;
    }




    public function store(){
        $query = "INSERT INTO `profilepicture` (`name`, `images`) VALUES ('".$this->name."', '".$this->img_name."');";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }

        else{

            Message::message("Error storing data");
            Utility::redirect("index.php");
        }
    }


    public function update(){
        if(!empty($this->img_name))
            $query = "UPDATE `profilepicture` SET `name` = '".$this->name."', `images` = '".$this->img_name."' WHERE `profilepicture`.`id` = ".$this->id;
        
        else
            $query = "UPDATE `profilepicture` SET `name` = '".$this->name."' WHERE `profilepicture`.`id` = ".$this->id;
        
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }
            
        else{

            Message::message("Error updating data");
            Utility::redirect("index.php");
        }
    }


    public function trash(){
        
        $query = "UPDATE `profilepicture` SET `deleted_at` = '".time()."' WHERE `profilepicture`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }

        else{

            Message::message("Error trashing data");
            Utility::redirect("index.php");
        }
    }


    public function recover(){

        $query = "UPDATE `profilepicture` SET `deleted_at`= NULL WHERE `profilepicture`.`id` = ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("trashlist.php");
        }

        else{

            Message::message("Error recovering data");
            Utility::redirect("trashlist.php");
        }
    }


    public function recoverSelected($IDs){
        //Utility::dd($IDs);
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "UPDATE `profilepicture` SET `deleted_at`= NULL WHERE `profilepicture`.`id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error recovering data");
                Utility::redirect("index.php");
            }
        }
        else
            Utility::redirect("trashlist.php");
    }
    
    
    public function delete(){
        $query = "DELETE FROM `profilepicture` WHERE `id`= ".$this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }

        else{

            Message::message("Error deleting data");
            Utility::redirect("index.php");
        }
    }


    public function deleteSelected($IDs){
        //Utility::dd($IDs);
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "DELETE FROM `profilepicture` WHERE `id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error deleting data");
                Utility::redirect("index.php");
            }
        }
        else
            Utility::redirect("trashlist.php");
    }

    public function trashed(){
        $allInfo = array();
        $query = "SELECT * FROM `profilepicture` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result))
            $allInfo[] = $row;

        return $allInfo;
    }
    
    
    
}