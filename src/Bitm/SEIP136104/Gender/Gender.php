<?php
namespace App\Bitm\SEIP136104\Gender;

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\Message\Message;

class Gender
{
    public $id = "";
    public $name="";
    public $gender = "";
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        if (array_key_exists("name", $data)) {
            $this->name= $data['name'];
        }


        if (array_key_exists("gender", $data)) {
            $this->gender = $data['gender'];
        }
    }


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "basis_atomicproject") or die("Database connection failed");
    }


    public function store(){
        $query="INSERT INTO `basis_atomicproject`.`gender` (`username`, `gender`) VALUES ('".$this->name."', '".$this->gender."')";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";

    }


    public function view()
    {
        $query = "SELECT * FROM `gender` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function update()
    {
        $query = "UPDATE `basis_atomicproject`.`gender` SET `username` = '" . $this->name . "', `gender` = '" . $this->gender . "' WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been updated successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function delete()
    {
        $query = "DELETE FROM `basis_atomicproject`.`gender` WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been deleted successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";

    }


    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `basis_atomicproject`.`gender` SET `deleted_at` =" . $this->deleted_at . " WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been sent to trash successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function trashed()
    {
        $_allgender = array();
        $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allgender[] = $row;
        }

        return $_allgender;
    }


    public function recover()
    {

        $query = "UPDATE `basis_atomicproject`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Data has been recovered successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }


    public function recoverSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `basis_atomicproject`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been recovered successfully");
                Utility::redirect("index.php");
            }

            else
                echo "ERROR!";
        }
    }


    public function deleteSelected($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `basis_atomicproject`.`gender` WHERE `gender`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }

            else
                echo "ERROR!";
        }
    }


    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `basis_atomicproject`.`gender` WHERE `deleted_at` IS NULL ";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }


    public function paginator($pageStartFrom=0,$Limit=5){
        $_allGender = array();
        $query="SELECT * FROM `gender` WHERE `deleted_at` IS NULL LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allGender[] = $row;
        }

        return $_allGender;
    }
}