<?php

namespace App\Bitm\SEIP136104\Message;

if(!isset($_SESSION['message']))
    session_start();



class Message{
    public static function message($data= NULL){
        if(!empty($data))
            self::setMessage($data);
        
        else{
            $_message = self::getMessage();
            return $_message;
        }
           
            
    }
    
    public static function getMessage(){
        $message = $_SESSION['message'];
        $_SESSION['message'] = '';
        return $message;
    }

    public static function setMessage($msg){
        $_SESSION['message'] = $msg;
    }
}
