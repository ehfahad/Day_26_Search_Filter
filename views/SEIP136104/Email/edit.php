<?php
include_once ('../../../vendor\autoload.php');

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\Email\Email;


$obj= new Email();
$obj->prepare($_GET);
$singleItem = $obj->view()
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Subscriber Details</h2> <br>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singleItem->id ?>">
            <label>Update Subscriber Name :</label>
            <input type="text" name="name" class="form-control"  placeholder="Username" value="<?php echo $singleItem->username?>"><br> <br>
            <label>Update Subscriber Name :</label>
            <input type="email" name="email" class="form-control"  placeholder="Email" value="<?php echo $singleItem->email ?>">
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

</body>
</html>
