<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Email\Email;
use App\Bitm\SEIP136104\Utility\Utility;

$obj=new Email();

if(array_key_exists("mark",$_POST))
    $obj->deleteSelected($_POST['mark']);
else
    Utility::redirect("trashlist.php");
