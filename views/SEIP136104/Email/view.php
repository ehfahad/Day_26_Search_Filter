<?php
    include_once ('../../../vendor\autoload.php');


    use App\Bitm\SEIP136104\Utility\Utility;
    use App\Bitm\SEIP136104\Email\Email;

    $obj= new Email();
    $obj->prepare($_GET);
    $singleBook = $obj->view()
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>

<body bgcolor="#5f9ea0">
<div class="container">
    <h2><?php echo $singleBook->username?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleBook->id ?></li>
        <li class="list-group-item">Username: <?php echo $singleBook->username ?></li>
        <li class="list-group-item">Email: <?php echo $singleBook->email ?></li>

        <br>

        <a href="index.php" class="btn btn-primary" role="button">Done</a>

    </ul>
</div>

</body>
</html>
