<!DOCTYPE html>
<html lang="en">
<head>
    <title>Subscriber Registration</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Subscriber Registration</h2>

    <form class="form-horizontal" role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Subscriber's Name :</label>
            <input type="text" class="form-control" name="name" placeholder="Enter Subscriber Name" >
        </div>

        <div class="form-group">
            <label for="email">Subscriber's Email :</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email">
        </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Register</button>
        </div>
    </form>
</div>

</body>
</html>

