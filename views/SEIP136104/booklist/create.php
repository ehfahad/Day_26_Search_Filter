<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add Book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
</head>
<body bgcolor="#5f9ea0">

<div class="container">
    <h2><center>Add Book Details</center></h2>
    <form class="form-horizontal" role="form" method="post" action="store.php">
        <div class="form-group">
            <label class="control-label col-sm-2" for="title">Book Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="title" placeholder="Enter Book Title">
            </div>
        </div>

        <div class="form-group">
            <label>Description:</label>
            <textarea class="form-control" rows="5" id="mytextarea" name="description"></textarea>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>

<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>

</body>
</html>
