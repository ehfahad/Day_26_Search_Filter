<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select Your Hobbies</h2>
    <form role="form" method="post" action="store.php">
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gaming">Gaming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Cricket">Playing Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Football" >Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Watching Movies" >Watching Movies</label>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>
</div>

</body>
</html>


