<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Hobby\Hobby;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new Hobby();
$obj->prepare($_GET);
$Hobby = $obj->view();
//Utility::d($Hobby);

//echo $_GET['id'];

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Your Hobbies</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gaming" <?php if(in_array("Gaming",$Hobby)) : ?> checked <?php endif; ?> >Gaming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Playing Cricket" <?php if(in_array("Playing Cricket",$Hobby)) : ?> checked <?php endif; ?>>Playing Cricket</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobby[]" value="Playing Football" <?php if(in_array("Playing Football",$Hobby)) : ?> checked <?php endif; ?> >Playing Football</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="hobby[]" value="Watching Movies" <?php if(in_array("Watching Movies",$Hobby)) : ?> checked <?php endif; ?> >Watching Movies</label>
        </div>
        <input type="submit" class="btn btn-primary" value="Update">
    </form>
</div>

</body>
</html>
