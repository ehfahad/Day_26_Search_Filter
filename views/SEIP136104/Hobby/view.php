<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Hobby\Hobby;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new Hobby();
$obj->prepare($_GET);
$Hobby = $obj->view();
//Utility::d($Hobby);

//echo $_GET['id'];

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Subscriber Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Subscriber Hobbies</h2>
    <form role="form">

        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" disabled value="Gaming" <?php if(in_array("Gaming",$Hobby)) : ?> checked <?php endif; ?> >Gaming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" disabled value="Playing Cricket" <?php if(in_array("Playing Cricket",$Hobby)) : ?> checked <?php endif; ?>>Playing Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" disabled value="Playing Football" <?php if(in_array("Playing Football",$Hobby)) : ?> checked <?php endif; ?> >Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" disabled value="Watching Movies" <?php if(in_array("Watching Movies",$Hobby)) : ?> checked <?php endif; ?> >Watching Movies</label>
        </div>
        <a href="index.php" class="btn btn-primary" role="button">Done</a>
    </form>
</div>

</body>
</html>
