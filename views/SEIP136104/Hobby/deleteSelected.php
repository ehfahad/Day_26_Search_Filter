<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Hobby\Hobby;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new Hobby();
$obj->prepare($_GET);

if(!empty($_POST['mark']))
$obj->deleteSelected($_POST['mark']);

else
    Utility::redirect("trashlist.php");
//Utility::d($Hobby);


?>
