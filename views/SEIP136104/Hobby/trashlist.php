<?php

session_start();
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Hobby\Hobby;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;


$obj =new Hobby();
$obj->prepare($_POST);
$allHobby = $obj->trashed();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby Trashlist</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <center><h2>Hobbies Trashlist</h2></center>
    <a href="index.php" class="btn btn-info" role="button">See All List</a>
    <form id="multiple" action="recoverSelected.php" method="post">
        <br>
        <button type="submit" class="btn btn-primary">Recover Selected</button>
        <button type="button"  onclick="ConfirmDelete()" class="btn btn-danger" id="multiple_delete">Delete Selected</button>

       <?php if(array_key_exists("message",$_SESSION) && (!empty($_SESSION['message']))): ?>
        <div class="alert alert-info" id="message">
            <center><?php echo Message::message() ?></center>
        </div>
        <?php endif; ?>
    <table class="table">
        <thead>
        <tr>
            <th>Check Item</th>
            <th>SL</th>
            <th>ID</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count =0 ;
        foreach ($allHobby as $item) { $count++;
            ?>
            <tr class="success">

                <td><input type="checkbox" name="mark[]" value="<?php echo $item->id ?>"></td>
                <td><?php echo $count?> </td>
                <td> <?php echo $item->id ?> </td>
                <td> <?php echo $item->hobby ?> </td>
                <td>
                    <a href="recover.php?id=<?php echo $item->id?>" class="btn btn-primary" role="button">Recover</a>
                    <a href="delete.php?id=<?php echo $item->id?>" class="btn btn-danger" onclick="return ConfirmDelete()" role="button">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    </form>
</div>
    <script>

        $('#message').show().delay(1200).fadeOut();


        function ConfirmDelete() {
            var x = confirm("Sure to delete?");
            if(x)
                return true;
            else
                return false;
        }
        
        
        $('#multiple_delete').on('click',function () {
            document.forms[0].action="deleteSelected.php";
            $('#multiple').submit();
        });



        
    </script>

</body>
</html>

