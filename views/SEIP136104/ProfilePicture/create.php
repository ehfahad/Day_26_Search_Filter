<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create Your Profile</h2>
    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Username :</label>
            <input type="text" class="form-control" placeholder="Enter Username" name="name">
        </div>
        <div class="form-group">
            <label>Upload Your Profile Picture :</label>
            <input type="file" class="form-control" placeholder="Insert File" name="image">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>

