<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\ProfilePicture\ImageUpload;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new ImageUpload();
$obj->prepare($_POST);
$singleInfo = $obj->view();

if((isset($_FILES['image'])&& !empty($_FILES['image']['name']))) {
    $img_name = time() . $_FILES["image"]['name'];
    $temp_location = $_FILES['image']['tmp_name'];

    unlink($_SERVER['DOCUMENT_ROOT'].'/FinalAtomicProject/Resources/Images/'.$singleInfo->images);

    move_uploaded_file($temp_location, 'C:/xampp/htdocs/FinalAtomicProject/Resources/Images/' . $img_name);
    $_POST['image'] = $img_name;
}

$obj->prepare($_POST);
$obj->update();



?>