<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\ProfilePicture\ImageUpload;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new ImageUpload();
$obj->prepare($_GET);
$singleInfo = $obj->view();
//Utility::dd($singleInfo)
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Profile</h2>
        <div class="form-group">
            <label>Username : </label><br>
            <input type="text" value="<?php echo $singleInfo->name ?>" disabled>
        </div>
        <div class="form-group">
            <label>Profile Picture : </label><br>
            <img src="../../../Resources/Images/<?php echo $singleInfo->images ?>" height="100px" width="100px">

        </div>

        <a href="index.php" role="button" class="btn btn-primary">Done</a>
</div>

</body>
</html>

