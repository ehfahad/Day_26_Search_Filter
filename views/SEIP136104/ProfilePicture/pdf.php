<?php

session_start();

include_once('../../../vendor/autoload.php');

require_once('../../../vendor/mpdf/mpdf/mpdf.php');

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\ProfilePicture\ImageUpload;

$obj = new ImageUpload();
$allData = $obj ->index();


$trs= "";
$sl=0;
foreach($allData as $data):
    $sl++;

    $trs.="<tr>";
    $trs.="<td>$sl</td>";
    $trs.="<td>$data->id</td>";
    $trs.="<td>$data->name</td>";
    $trs.="<td> <img src='../../../Resources/Images/$data->images'></td>";
    $trs.="</tr>";

endforeach;

$html = <<<heredoc
<table class="table">
        <thead>
        <tr>
            <td>SL</td>
            <td>ID</td>
            <td>Name</td>
            <td>Profile Picture</td>
        </tr>
        </thead>

        <tbody>
            $trs;
        </tbody>
    </table>
heredoc;

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output("Booklist.pdf","D");