<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\ProfilePicture\ImageUpload;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new ImageUpload();
$obj->prepare($_GET);
$singleInfo = $obj->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Your Profile</h2>
    <form role="form" action="update.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" >
        <div class="form-group">
            <label>Update Username:</label>
            <input type="text" class="form-control" value="<?php echo $singleInfo->name?>" name="name">
        </div>
        <div class="form-group">
            <label>Update Your Profile Picture:</label>
            <input type="file" class="form-control" name="image">
            <img src="../../../Resources/Images/<?php echo $singleInfo->images ?>" height="100px" width="100px">
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

</body>
</html>

