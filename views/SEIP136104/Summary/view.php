<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Summary\Summary;
use App\Bitm\SEIP136104\Utility\Utility;


$obj=new Summary();
$obj->prepare($_GET);
$single = $obj->view();
//Utility::debug($single);
?>


<html>
<head>
    <title>View Organization Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Organization Details</h2>
    <label>Organization Name :</label> <br>
    <input type="text" name="name" disabled value="<?php echo $single->org_name ?>"><br><br>
    <label>Organization Summary :</label><br>
    <textarea class="form-control" rows="5" name="summary" disabled><?php echo $single->summary ?></textarea><br>
    <a href="index.php" role="button" class="btn btn-primary">Done</a>
    </form>
</div>

</body>
</html>


