<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Summary\Summary;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$obj = new Summary();
$obj->prepare($_GET);
$single = $obj->view();


?>



<html>
<head>
    <title>Update Organization Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Organization Details</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <label>Update Organization Name :</label> <br>
        <input type="text" name="name" placeholder="Enter your name" value="<?php echo $single->org_name ?>"><br><br>
        <label>Update Organization Summary :</label><br>
        <div class="checkbox">
            <textarea class="form-control" rows="5" name="summary"><?php echo $single->summary ?></textarea>
        </div>

        <input type="submit" class="btn btn-primary" value="Update">
    </form>
</div>

</body>
</html>
