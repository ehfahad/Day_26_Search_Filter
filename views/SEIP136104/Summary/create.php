<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add Summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Organization Summary</h2>
    <form role="form" method="post" action="store.php">
        <label>Organization Name : </label> <br>
        <input type="text" name="name" placeholder="Enter organization name"> <br> <br>
        <div class="form-group">
            <label for="comment">Write Organization Summary :</label>
            <textarea class="form-control" rows="5" name="summary" placeholder="Enter summary"></textarea>
        </div>
        <input type="submit" class="btn btn-primary" value="Create">
    </form>
</div>

</body>
</html>
