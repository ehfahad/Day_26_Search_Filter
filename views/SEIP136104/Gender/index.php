<?php
session_start();

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Gender\Gender;
use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\Message\Message;

if(array_key_exists("itemPerPage",$_SESSION)) {
    if(array_key_exists("itemPerPage",$_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
}
else
    $_SESSION['itemPerPage']=5;

$itemPerPage=$_SESSION['itemPerPage'];

$obj =new Gender();
$totalItem=$obj->count();
//echo $totalItem;
$totalPage=ceil($totalItem/$itemPerPage);

if(array_key_exists("pageNumber",$_GET))
    $pageNumber=$_GET['pageNumber'];
else
    $pageNumber=1;

$pagination= "";
for($count=1;$count<=$totalPage;$count++){
    $class=($pageNumber==$count)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom= $itemPerPage*($pageNumber-1);

$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


?>
<html>
<head>
    <title>Users Informations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">Atomic Project</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="../../../index.php">Home</a></li>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->


<div class="container">
    <h2>Manage Users Details</h2>
    <?php if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])): ?>
        <div id="message" class="alert alert-info">
            <center> <?php echo Message::message() ?></center>
        </div>
    <?php endif; ?>
    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-primary" role="button">View Trashlist</a>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5): ?> selected <?php endif ?>>5</option>
                <option <?php if($itemPerPage==10): ?> selected <?php endif ?> >10</option>
                <option <?php if($itemPerPage==15): ?> selected <?php endif ?>>15</option>
                <option <?php if($itemPerPage==20): ?> selected <?php endif ?>>20</option>
                <option <?php if($itemPerPage==25): ?> selected <?php endif ?>>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <table class="table">
        <thead>
        <tr>
            <td>SL</td>
            <td>ID</td>
            <td>Username</td>
            <td>Gender</td>
            <td>Action</td>
        </tr>
        </thead>

        <tbody>
        <?php $sl=$pageStartFrom;
        foreach($allInfo as $info){ $sl++;  ?>
            <tr class="success">
                <td><?php echo $sl ?></td>
                <td><?php echo $info->id?></td>
                <td><?php echo $info->username ?></td>
                <td><?php echo $info->gender ?></td>
                <td>
                    <a href="view.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Update</a>
                    <a href="trash.php?id=<?php echo $info->id ?>" class="btn btn-warning" role="button">Trash</a>
                    <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" onclick="return ConfirmDelete()" role="button">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <ul class="pagination">
        <?php if($pageNumber>1):?> <li><a href='index.php?pageNumber=<?php echo $pageNumber-1 ?>'> Prev </a></li> <?php endif;?>
        <?php echo $pagination; ?>
        <?php if($pageNumber<$totalPage):?> <li><a href='index.php?pageNumber=<?php echo $pageNumber+1 ?>'> Next </a></li> <?php endif;?>
    </ul>

</div>

<script>
    $('#message').show().delay(1500).fadeOut();

    function ConfirmDelete(){
        var x=confirm("Sure to delete?");
        if(x)
            return true;
        else
            return false;
    }
</script>
</body>
</html>