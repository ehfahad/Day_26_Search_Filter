<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Gender\Gender;

$gender= new Gender();
$gender->prepare($_GET);
$singleItem=$gender->view();

?>

<!DOCTYPE html>

<html>
<head>
    <title>Select Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <h2>Update Your Details</h2> <br>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Update Username:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" name="name" class="form-control"  value="<?php echo $singleItem->username?>">
        </div>


    <label>Update User Gender : </label>
    <div class="checkbox">
        <label><input type="radio" name="gender" value="Male" <?php if($singleItem->gender=="Male"):?>checked<?php endif ?>>Male</label>
    </div>
    <div class="checkbox">
        <label><input type="radio" name="gender" value="Female"  <?php if($singleItem->gender=="Female"):?>checked<?php endif ?> >Female</label>
    </div>
    <div class="checkbox">
        <label><input type="radio" name="gender" value="Others"  <?php if($singleItem->gender=="Others"):?>checked<?php endif ?>>Others</label>
    </div>
    <br/>
    <button type="submit" class="btn btn-primary">Update</button>

    </form>
</div>

</body>
</html>





