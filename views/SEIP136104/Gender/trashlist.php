<?php
session_start();

include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP136104\Gender\Gender;
use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\Message\Message;

$obj= new Gender();
$allInfo= $obj->trashed();

?>
<html>
<head>
    <title>Gender Trashlist</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <center><h2>User Trashlist</h2></center>
    <?php if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])): ?>
        <div id="message" class="alert alert-info">
            <center> <?php echo Message::message() ?></center>
        </div>
    <?php endif; ?>

    <a href="index.php" class="btn btn-primary" role="button">View All List</a>

    <form id="multiple" action="recoverSelected.php" method="post">
        <br>
        <input type="submit" value="Recover Selected" role="button" class="btn btn-primary">
        <input type="submit" id="delete_multiple" onclick="return ConfirmDelete()" value="Delete Selected" role="button" class="btn btn-danger">
        <table class="table">
            <thead>
            <tr>
                <td>Check Item</td>
                <td>SL</td>
                <td>ID</td>
                <td>Username</td>
                <td>Gender</td>
                <td>Action</td>
            </tr>
            </thead>

            <tbody>
            <?php $sl=0;
            foreach($allInfo as $info){ $sl++ ?>
                <tr>
                    <td>
                        <input type="checkbox" name="mark[]" value="<?php echo $info->id ?>">
                    </td>
                    <td><?php echo $sl ?></td>
                    <td><?php echo $info->id?></td>
                    <td><?php echo $info->username?></td>
                    <td><?php echo $info->gender?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $info->id ?>" onclick="return ConfirmDelete()"  class="btn btn-danger" role="button">Delete</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>
</div>

<script>
    $('#message').show().delay(1500).fadeOut();

    function ConfirmDelete(){
        var x=confirm("Sure to delete?");
        if(x)
            return true;
        else
            return false;
    };

    $('#delete_multiple').on('click',function(){
        document.forms[0].action="deleteSelected.php";
        $('#multiple').submit();
    });
</script>
</body>
</html>


