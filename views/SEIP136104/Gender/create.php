<!DOCTYPE html>
<html>
<head>
    <title>Select Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Enter Your Details</h2>
    <form role="form" method="post" action="store.php">

        <div>
            <label>Enter Your Name : </label> <br>
            <input type="text" name="name" placeholder="Enter your name"><br>
        </div>

        <br> <br>
        <label>Select Your Gender : </label> <br>
        <div class="checkbox">
            <label><input type="radio" name="gender" value="Male">Male</label>
        </div>
        <div class="checkbox">
            <label><input type="radio" name="gender" value="Female" >Female</label>
        </div>
        <div class="checkbox">
            <label><input type="radio" name="gender" value="Others" >Others</label>
        </div>
        <br/>
        <button type="submit" class="btn btn-primary">Create</button>

    </form>
</div>

</body>
</html>

