<?php

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Birthday\Birthday;
use App\Bitm\SEIP136104\Utility\Utility;


$obj=new Birthday();
$obj->prepare($_GET);
$single = $obj->view();
//Utility::debug($single);
?>




<html>
<head>
    <title>Edit Birthday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update User Details</h2>
    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <label>Update User Name :</label> <br>
        <input type="text" name="name" placeholder="Enter your name" value="<?php echo $single->name ?>"><br><br>
        <label>Update User Birthday :</label><br>
        <div class="checkbox">
            <label><input type="date" name="date" value="<?php echo $single->date ?>"> </label>
        </div>

        <input type="submit" class="btn btn-primary" value="Update">
    </form>
</div>

</body>
</html>


