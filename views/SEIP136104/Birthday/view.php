<?php

include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP136104\Birthday\Birthday;
use App\Bitm\SEIP136104\Utility\Utility;


$obj=new Birthday();
$obj->prepare($_GET);
$single = $obj->view();
//Utility::debug($single);
?>


<html>
<head>
    <title>View Birthday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>User Birthday</h2>
        <label>Username :</label> <br>
        <input type="text" name="name" value="<?php echo $single->name ?>"><br><br>
        <label>User's birthday :</label><br>
        <label><input type="text" name="date" value="<?php echo date("d-m-Y",strtotime($single->date)) ?>"> </label><br>

    <a href="index.php" role="button" class="btn btn-primary">Done</a>
    </form>
</div>

</body>
</html>


