<html>
<head>
    <title>Add Birthday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Your Birthday Details</h2>
    <form role="form" method="post" action="store.php">
        <label>Enter Your Name :</label> <br>
        <input type="text" name="name" placeholder="Enter your name"><br><br>
        <label>Enter Your Birthday :</label><br>
        <div class="checkbox">
            <label><input type="date" name="date"> </label>
        </div>

        <input type="submit" value="Submit" class="btn btn-primary">
    </form>
</div>

</body>
</html>


