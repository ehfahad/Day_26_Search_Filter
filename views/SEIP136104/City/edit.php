<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\City\City;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;


$object= new City();
$object->prepare($_GET);
$singleItem=$object->view();

//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Update Users Info</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Update Your Name:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" name="name" class="form-control"  value="<?php echo $singleItem->username?>">
        </div>
        <div class="form-group">
            <label for="sel1">Update Your City</label>
            <select class="form-control" id="sel1" name="city">
                <option <?php if($singleItem->city=="Select One"):?>selected<?php endif ?>>Select One</option>
                <option <?php if($singleItem->city=="Barisal"):?>selected<?php endif ?>>Barisal</option>
                <option <?php if($singleItem->city=="Chittagong"):?>selected<?php endif ?>>Chittagong</option>
                <option <?php if($singleItem->city=="Dhaka"):?>selected<?php endif ?>>Dhaka</option>
                <option <?php if($singleItem->city=="Khulna"):?>selected<?php endif ?>>Khulna</option>
                <option <?php if($singleItem->city=="Rajshahi"):?>selected<?php endif ?>>Rajshahi</option>
                <option <?php if($singleItem->city=="Rangpur"):?>selected<?php endif ?>>Rangpur</option>
                <option <?php if($singleItem->city=="Sylhet"):?>selected<?php endif ?>>Sylhet</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

</body>
</html>
