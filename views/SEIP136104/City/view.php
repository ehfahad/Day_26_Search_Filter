<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\City\City;
use App\Bitm\SEIP136104\Message\Message;
use App\Bitm\SEIP136104\Utility\Utility;

$info= new City();
$info->prepare($_GET);
$singleItem=$info->view();
//Utility::d($singleItem);
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>View City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2><?php echo $singleItem->username?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">User Name: <?php echo $singleItem->username?></li>
        <li class="list-group-item">City: <?php echo $singleItem->city?></li>
    </ul>
    <br>
    <a href="index.php" role="button" class="btn btn-primary">Done</a>
</div>

</body>
</html>