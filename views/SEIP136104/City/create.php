<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <h2>Add User City</h2>

    <form role="form" method="post" action="store.php">

        <label>Username :</label> <br>
        <input type="text" name="name"><br><br>

        <div class="form-group">
            <label for="sel1">Select Your City</label>
            <select class="form-control" id="sel1" name="city">
                <option>Select One</option>
                <option>Barisal</option>
                <option>Chittagong</option>
                <option>Dhaka</option>
                <option>Khulna</option>
                <option>Rajshahi</option>
                <option>Rangpur</option>
                <option>Sylhet</option>
            </select>
        </div>

        <input type="submit" class="btn btn-primary" value="Create">
    </form>
</div>

</body>
</html>
